from __future__ import division
import pandas as pd
import numpy as np

def preprocess_data(full_data):
    if 'state' in full_data.columns:
        full_data = full_data.drop('state',axis = 1)

    # Convert String dates to Datetime
    full_data['launched_date']=pd.to_datetime(full_data['launched'],format="%Y/%m/%d %H:%M").dt.normalize()
    full_data['deadline_date']=pd.to_datetime(full_data['deadline'],format="%Y/%m/%d")

    # Obtain WeekDay, Month and Year for Launched Dates
    full_data['launched_week_day']=full_data['launched_date'].dt.weekday_name
    full_data['launched_month']=full_data['launched_date'].dt.month
    full_data['launched_year']=full_data['launched_date'].dt.year

    # Obtain WeekDay, Month and Year for Deadline Dates
    full_data['deadline_week_day']=full_data['deadline_date'].dt.weekday_name
    full_data['deadline_month']=full_data['deadline_date'].dt.month
    full_data['deadline_year']=full_data['deadline_date'].dt.year

    # Calculate Time Period
    full_data['time_period'] = full_data['deadline_date'] - full_data['launched_date']

    full_data['goal_by_time'] = full_data['usd_goal_real']/full_data['time_period'].dt.days

    full_data['time_period'] = pd.to_numeric(full_data['time_period'], downcast = 'float')

    # Feature : Category
    category_counts = full_data['category'].value_counts()

    # Tiers for sparser studios
    one_K_timers = category_counts[category_counts <= 1000].index
    five_K_timers = category_counts[(category_counts > 1000) & (category_counts <= 5000)].index
    ten_K_timers = category_counts[(category_counts > 5000) & (category_counts <= 10000)].index
    fifteen_K_timers = category_counts[(category_counts > 10000) & (category_counts <= 15000)].index
    fifteen_K_above = category_counts[category_counts > 15000].index

    # Combine sparse studios
    full_data['category'].replace(one_K_timers, 'One K Timer', inplace=True)
    full_data['category'].replace(five_K_timers, 'Five K Timer', inplace=True)
    full_data['category'].replace(ten_K_timers, 'Ten K Timer', inplace=True)
    full_data['category'].replace(fifteen_K_timers, 'Fifteen K Timer', inplace=True)
    full_data['category'].replace(fifteen_K_above, 'Fifteen K Above', inplace=True)

    # Feature : Main Category
    main_category_counts = full_data['main_category'].value_counts()
    #print(main_category_counts)

    # Tiers for sparser studios
    ten_K_timers = main_category_counts[main_category_counts <= 10000].index
    twenty_K_timers = main_category_counts[(main_category_counts > 10000) & (main_category_counts <= 20000)].index
    thirty_K_timers = main_category_counts[(main_category_counts > 20000) & (main_category_counts <= 30000)].index
    forty_K_timers = main_category_counts[(main_category_counts > 30000) & (main_category_counts <= 40000)].index
    forty_K_above = main_category_counts[(main_category_counts > 40000)].index

    # Combine sparse studios
    full_data['main_category'].replace(ten_K_timers, 'Ten K Timer', inplace=True)
    full_data['main_category'].replace(twenty_K_timers, 'Twenty K Timer', inplace=True)
    full_data['main_category'].replace(thirty_K_timers, 'Thirty K Timer', inplace=True)
    full_data['main_category'].replace(forty_K_timers, 'Forty K Timer', inplace=True)
    full_data['main_category'].replace(forty_K_above, 'Forty K Above', inplace=True)

    # Feature : Currency
    currency_counts = full_data['currency'].value_counts()

    # Tiers for sparser studios
    ten_K_timers = currency_counts[currency_counts <= 10000].index
    forty_K_timers = currency_counts[(currency_counts > 10000) & (currency_counts <= 40000)].index
    forty_K_above = currency_counts[(currency_counts > 30000)].index

    # Combine sparse studios
    full_data['currency'].replace(ten_K_timers, 'Others', inplace=True)
    full_data['currency'].replace(forty_K_timers, 'British Pound, Euro, Canadian Dollar', inplace=True)
    full_data['currency'].replace(forty_K_above, 'USD', inplace=True)

    # Feature : Country
    country_counts = full_data['country'].value_counts()

    # Tiers for sparser studios
    five_K_timers = country_counts[country_counts <= 5000].index
    ten_K_timers = country_counts[(country_counts > 5000) & (country_counts <= 10000)].index
    forty_K_timers = country_counts[(country_counts > 10000) & (country_counts <= 40000)].index
    forty_K_above = country_counts[country_counts > 40000].index

    # Combine sparse studios
    full_data['country'].replace(five_K_timers, 'Five K Timer', inplace=True)
    full_data['country'].replace(ten_K_timers, 'Ten K Timer', inplace=True)
    full_data['country'].replace(forty_K_timers, 'Britain, Canada', inplace=True)
    full_data['country'].replace(forty_K_above, 'USD', inplace=True)

    test_data = pd.get_dummies (full_data.drop(['ID', 'name','usd pledged' ,'launched_date', 'deadline_date','launched_week_day','deadline_week_day','deadline','launched','pledged','goal'], axis=1) )

    return test_data