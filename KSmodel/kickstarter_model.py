import pickle
import os

def kickstarter_model(test_data):
# Load the model from disk
    folder = os.path.dirname(os.path.abspath( __file__ ))
    path = folder + "\\kickstarter_randomforest_model.sav"
    loaded_model = pickle.load(open(path, 'rb'))
    predictions = loaded_model.predict(test_data)
    test_data['state'] = predictions
    return test_data
    
 