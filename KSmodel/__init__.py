from __future__ import division
import pickle

from KSmodel.kickstarter_model import kickstarter_model
from KSmodel.preprocessing_data import preprocess_data