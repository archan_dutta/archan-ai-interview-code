Here are the solutions to technical exercise.


1. **KSmodel** is the Python Module. It consists of multiple python files. 


2. **Answers.mp3** is an audio file with solutions to the 14 questions.


3. **KSModel Documentation** elaborates on how to use KSModel.


4. **Nexus Edge Data Challenge** is the Python - Jupyter notebook that contains the details of the analysis.


To Use KSModel and import it successfully using you custom Python file, your custom Python file and KSModel SHOULD BE in the SAME FOLDER.


The module **cannot** be used system-wide right now, so please follow the above instructions and the follow the KSModel Documentation.

NOTE: Due to technical issue, I had to work on **Python 2.7.13** and **Anaconda 4.3.1**
